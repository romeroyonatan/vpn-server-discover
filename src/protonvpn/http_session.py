import requests
from requests.adapters import HTTPAdapter
from urllib3 import Retry


def get_session(max_retries=3, timeout_seconds=30):
    """Build a requests.Session with retries and timeout"""
    session = requests.Session()

    retry_strategy = Retry(
        total=max_retries,
        status_forcelist=[429, 500, 502, 503, 504],  # retry only on these error cores
        allowed_methods=["HEAD", "GET", "OPTIONS"],  # retry only on these http methods
    )
    adapter = TimeoutHTTPAdapter(timeout=timeout_seconds, max_retries=retry_strategy)
    session.mount("https://", adapter)
    session.mount("http://", adapter)

    return session


DEFAULT_TIMEOUT = 30  # seconds


class TimeoutHTTPAdapter(HTTPAdapter):
    """Add timeout to all requests to avoid block forever.

    See: https://findwork.dev/blog/advanced-usage-python-requests-timeouts-retries-hooks/
    """

    def __init__(self, *args, timeout=DEFAULT_TIMEOUT, **kwargs):
        super().__init__(*args, **kwargs)
        self.timeout = timeout

    def send(self, request, **kwargs):
        kwargs.setdefault("timeout", self.timeout)
        return super().send(request, **kwargs)
