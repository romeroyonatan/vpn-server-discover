# ruff: noqa: E402
import gi
gi.require_version("Gtk", "3.0")

import enum
import functools
import logging
import operator

from protonvpn.protonvpn import ProtonVPN, ResultSet
from protonvpn.servers import Feature


from gi.repository import Gtk  # noqa
import pathlib

ASSETS_DIR = pathlib.Path(__file__).parent / "assets"
UI_DIR = ASSETS_DIR / "ui"

ITEMS_PER_PAGE = 12

class OrderBy(enum.Enum):
    NAME = "Name"
    SCORE = "Score"
    LOAD = "Load"

    def get_criteria(self):
        if self == OrderBy.NAME:
            return lambda server: server.name
        elif self == OrderBy.SCORE:
            return lambda server: server.score
        elif self == OrderBy.LOAD:
            return lambda server: server.load


@Gtk.Template(filename=UI_DIR / "main.ui")
class ServersWindow(Gtk.Window):
    LABEL_ALL = "All"

    __gtype_name__ = "servers"
    _proton_vpn: ProtonVPN
    _servers: ResultSet
    _feature_filter: list

    server_list = Gtk.Template.Child()
    filter_country = Gtk.Template.Child()
    box_features = Gtk.Template.Child()

    def _load_servers(self):
        self._servers = self._proton_vpn.get_servers()
        self._render_servers()

    def _render_servers(self):
        self._clean_server_list()
        servers = self._sort_and_filter()

        # TODO Pagination
        page = list(servers)[:ITEMS_PER_PAGE]
        for server in page:
            row = self._build_server_row(server)
            self.server_list.add(row)
        self.server_list.show_all()

    def _sort_and_filter(self):
        servers = self._servers

        if self._feature_filter:
            features = functools.reduce(operator.or_, self._feature_filter)
            servers = servers.filter(lambda server: features in server.features)
        if self._active_country:
            servers = servers.filter(
                lambda server: server.exit_country == self._active_country
                or self._active_country == self.LABEL_ALL
            )
        if self._search_term:
            servers = servers.filter(
                lambda server: server.name.upper() == self._search_term
                or server.exit_country.upper() == self._search_term
            )
        if self._order_by:
            servers = servers.sort_by(self._order_by.get_criteria())
        return servers

    def _build_server_row(self, server):
        # FIXME: Can I use a template?
        row = Gtk.ListBoxRow()
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=50)
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        hbox.pack_start(vbox, True, True, 0)
        name = Gtk.Label(label=server.name)
        vbox.pack_start(name, True, True, 0)
        country = Gtk.Label(label=server.exit_country)
        vbox.pack_start(country, True, True, 0)
        load = Gtk.Label(label=f"Load: {server.load}")
        hbox.pack_start(load, True, True, 0)
        score = Gtk.Label(label=f"Score: {server.score:.2f}")
        hbox.pack_start(score, True, True, 0)
        row.add(hbox)
        return row

    def _clean_server_list(self):
        self.server_list.foreach(lambda row: self.server_list.remove(row))

    @Gtk.Template.Callback()
    def on_window_show(self, window):
        self._proton_vpn = ProtonVPN()
        self._feature_filter = []
        self._active_country = self.LABEL_ALL
        self._search_term = ""
        self._order_by = None

        self._load_servers()
        self._load_countries()
        self._load_features()

    @Gtk.Template.Callback()
    def on_refresh_button_clicked(self, button):
        self._proton_vpn.refresh()
        self._load_servers()

    def _load_countries(self):
        countries = sorted({server.exit_country for server in self._servers})
        self.filter_country.append_text(self.LABEL_ALL)
        self.filter_country.set_active(0)
        for country in countries:
            self.filter_country.append_text(country)

    def _load_features(self):
        for feature in Feature:
            feature_check = Gtk.CheckButton(label=feature.name)
            feature_check.connect("toggled", self.on_feature_check_toggled, feature)
            self.box_features.pack_start(feature_check, True, True, 0)
        self.box_features.show_all()

    def on_feature_check_toggled(self, feature_check, feature):
        active = feature_check.get_active()
        if active:
            self._feature_filter.append(feature)
        else:
            self._feature_filter.remove(feature)
        self._render_servers()

    @Gtk.Template.Callback()
    def on_search_input_changed(self, search_entry):
        self._search_term = search_entry.get_text().upper()
        self._render_servers()

    @Gtk.Template.Callback()
    def on_filter_country_changed(self, filter_country):
        self._active_country = filter_country.get_active_text()
        self._render_servers()

    @Gtk.Template.Callback()
    def on_order_by_changed(self, order_by):
        self._order_by = OrderBy(order_by.get_active_text())
        self._render_servers()


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="ch.proton.app", **kwargs)
        self.window = None

    def do_activate(self):
        self.window = ServersWindow(application=self)
        self.window.present()


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s %(filename)s %(levelname)s - %(message)s"
    )
    app = Application()
    app.run()


if __name__ == "__main__":
    main()
