from typing import Callable, Iterable, Self

from protonvpn.cache import Cache
from protonvpn.clients import ProtonApiClient, ProtonHTTPApiClient
from protonvpn.servers import LogicalServer


class ResultSet:
    def __init__(self, data: Iterable[LogicalServer]):
        self._data = data

    def __iter__(self):
        return iter(self._data)

    def filter(self, a_filter: Callable[[LogicalServer], bool]) -> Self:
        return ResultSet(item for item in self._data if a_filter(item))

    def sort_by(self, criteria: Callable[[LogicalServer], bool]) -> Self:
        return ResultSet(item for item in sorted(self._data, key=criteria))

    def as_list(self) -> list[LogicalServer]:
        return list(self)


class ProtonVPN:
    """Expose the VPN services.

    This is the main entrypoint to get data from Proton
    """

    def __init__(self, *, client: ProtonApiClient = None):
        client = client or ProtonHTTPApiClient()
        self._servers_cache = Cache(client.get_servers)

    def get_servers(self) -> ResultSet:
        """Get the available Logical VPN Servers.

        Usage::

            (
                ProtonVPN()
                    .get_servers()
                    .filter(lambda server: server.is_online())
                    .sort_by(lambda server: server.name)
                    .as_list()
            )

        """
        return ResultSet(self._servers_cache.get())

    def refresh(self):
        """Refresh the server list"""
        self._servers_cache.refresh()
