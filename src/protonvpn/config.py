"""Configuration source

I hold the customizable parameters. I read the parameters from environment variables or files

The precedence is:
* Environment variable
* User file settings

Environment Variables:
- PROTONVPN_BASE_URL: The base URL of the Proton VPN API server [default: https://api.protonvpn.ch]
- PROTONVPN_CACHE_SECONDS: The amount of seconds which we hold the cache [default: 600]

User settings:
Default location: $HOME/.config/protonvpn/config.ini

Example::
    [ProtonVPN]
    base_url = "https://api.protonvpn.ch
    cache_seconds = 600

"""
import configparser
import os
import pathlib

DEFAULT_EXPIRATION_SECONDS = 600
DEFAULT_BASE_URL = "https://api.protonvpn.ch"

ENV_VAR_PREFIX = "PROTONVPN"
CONFIG_SECTION = "ProtonVPN"

USER_SETTINGS_PATH = pathlib.Path.home() / ".config" / "protonvpn" / "config.ini"


def get_cache_seconds(default=DEFAULT_EXPIRATION_SECONDS) -> int:
    return int(_read_config("cache_seconds", default))


def get_base_url(default=DEFAULT_BASE_URL) -> str:
    return _read_config("base_url", default)


def _read_config(option_name, default) -> str:
    value = os.getenv(f"{ENV_VAR_PREFIX}_{option_name.upper()}")
    if value:
        return value

    if USER_SETTINGS_PATH.exists():
        parser = configparser.ConfigParser()
        parser.read(USER_SETTINGS_PATH)
        value = parser.get(CONFIG_SECTION, option_name, fallback=default)
        if value:
            return value

    return default
