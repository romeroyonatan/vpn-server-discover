from pydantic import BaseModel, Field


class PhysicalServerSchema(BaseModel):
    ID: str = Field(max_length=255)
    EntryIP: str = Field(max_length=16)
    ExitIP: str = Field(max_length=16)
    Domain: str = Field(max_length=255)
    Status: int = Field(ge=0, le=1)


class CoordinatesSchema(BaseModel):
    Lat: float
    Long: float


class LogicalServerSchema(BaseModel):
    ID: str = Field(max_length=255)
    Name: str = Field(max_length=255)
    EntryCountry: str = Field(max_length=3)
    ExitCountry: str = Field(max_length=3)
    HostCountry: str | None = Field(max_length=3)
    Domain: str = Field(max_length=255)
    Tier: int = Field(ge=0, le=2)
    Features: int = Field(ge=0, le=64)
    Status: int = Field(ge=0, le=1)
    Region: str | None = Field(max_length=255)
    City: str | None = Field(max_length=255)
    Servers: list[PhysicalServerSchema] = Field(max_length=255)
    Load: int = Field(ge=0, le=100)
    Location: CoordinatesSchema
    Score: float = Field(ge=0, le=100)
