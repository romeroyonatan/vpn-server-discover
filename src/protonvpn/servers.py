import dataclasses
import enum
from typing import Self

from protonvpn import validation


class Feature(enum.Flag):
    SECURE_CORE = 1
    TOR = 2
    P2P = 4
    STREAMING = 8
    IPV6 = 16
    RESTRICTED = 32


@dataclasses.dataclass(frozen=True)
class PhysicalServer:
    id: str
    entry_ip: str
    exit_ip: str
    domain: str
    status: int

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        cleaned_data = validation.PhysicalServerSchema.model_validate(a_dict)
        return cls(
            id=cleaned_data.ID,
            entry_ip=cleaned_data.EntryIP,
            exit_ip=cleaned_data.ExitIP,
            domain=cleaned_data.Domain,
            status=cleaned_data.Status,
        )


@dataclasses.dataclass(frozen=True)
class Coordinates:
    latitude: float
    longitude: float

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        cleaned_data = validation.CoordinatesSchema.model_validate(a_dict)
        return cls(
            latitude=cleaned_data.Lat,
            longitude=cleaned_data.Long,
        )


@dataclasses.dataclass(frozen=True)
class LogicalServer:
    id: str
    name: str
    entry_country: str
    exit_country: str
    host_country: str | None
    domain: str
    tier: int
    features: Feature
    status: int
    region: str | None
    city: str | None
    servers: list[PhysicalServer]
    load: int
    location: Coordinates
    score: float

    @classmethod
    def from_dict(cls, a_dict: dict) -> Self:
        cleaned_data = validation.LogicalServerSchema.model_validate(a_dict)
        return cls(
            id=cleaned_data.ID,
            name=cleaned_data.Name,
            entry_country=cleaned_data.EntryCountry,
            exit_country=cleaned_data.ExitCountry,
            host_country=cleaned_data.HostCountry,
            domain=cleaned_data.Domain,
            tier=cleaned_data.Tier,
            features=Feature(cleaned_data.Features),
            status=cleaned_data.Status,
            region=cleaned_data.Region,
            city=cleaned_data.City,
            servers=[
                PhysicalServer.from_dict(server_dict)
                for server_dict in a_dict["Servers"]
            ],
            load=cleaned_data.Load,
            location=Coordinates.from_dict(a_dict["Location"]),
            score=cleaned_data.Score,
        )

    def is_online(self):
        return self.status == 1
