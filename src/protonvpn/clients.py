import logging
from abc import abstractmethod, ABCMeta

from protonvpn import config
from protonvpn.http_session import get_session
from protonvpn.servers import LogicalServer

logger = logging.getLogger(__name__)


class ProtonApiClient(metaclass=ABCMeta):
    """I can communicate with the ProtonVPN API.

    I hide the complexities of data serialization and error handling.
    """

    @abstractmethod
    def get_servers(self) -> list[LogicalServer]:
        """Get a list of available logical servers"""


class ProtonHTTPApiClient(ProtonApiClient):
    def __init__(self, base_url: str = ""):
        base_url = base_url or config.get_base_url()
        self._base_url = base_url.removesuffix("/")
        self._session = get_session()

    def get_servers(self) -> list[LogicalServer]:
        url = f"{self._base_url}/vpn/logicals"
        logger.info("Getting VPN logical servers. url=%r", url)
        response = self._session.get(url)
        logger.debug("Response %r", response)
        response.raise_for_status()
        data = response.json()
        # FIXME: I would like to check the Code, but I don't know the meaning and the different codes.
        #  Is it an error code?
        servers = [
            LogicalServer.from_dict(logical_server_dict)
            for logical_server_dict in data["LogicalServers"]
        ]
        return servers
