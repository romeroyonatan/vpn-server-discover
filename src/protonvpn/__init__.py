"""Proton VPN Client

Discover Proton's VPN servers.
"""

from .protonvpn import ProtonVPN

__all__ = ["ProtonVPN"]
