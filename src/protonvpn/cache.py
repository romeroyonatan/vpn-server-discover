import logging
from datetime import datetime, timedelta
from typing import Callable, Any

from protonvpn import config

logger = logging.getLogger(__name__)

_UNSET_EXPIRATION_SECONDS = -32767


class Cache:
    def __init__(
        self,
        source: Callable[[], Any],
        *,
        expiration_seconds: int = _UNSET_EXPIRATION_SECONDS
    ):
        if expiration_seconds == _UNSET_EXPIRATION_SECONDS:
            expiration_seconds = config.get_cache_seconds()

        self._source = source
        self._data = None
        self._expiration_seconds = expiration_seconds
        self._expiration_date = datetime.min

    def get(self):
        if self.is_expired():
            logger.debug("Cache expired")
            self._get_data_from_source()
        else:
            logger.info("Retrieving data from cache")
        return self._data

    def refresh(self):
        self._get_data_from_source()

    def _get_data_from_source(self):
        logger.debug("Retrieving data from source")
        self._data = self._source()
        self._expiration_date = datetime.now() + timedelta(
            seconds=self._expiration_seconds
        )

    def is_expired(self):
        return self._data is None or self._expiration_date <= datetime.now()
