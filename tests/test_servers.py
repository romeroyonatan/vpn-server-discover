import pytest

from protonvpn import ProtonVPN
from protonvpn.servers import Feature
from tests import examples
from tests.fakes import FakeProtonApiClient


def test_get_all_servers():
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_SERVER)

    proton_vpn = ProtonVPN(client=fake_client)

    actual = proton_vpn.get_servers().as_list()
    expected = [examples.A_SERVER]
    assert actual == expected


@pytest.mark.parametrize(
    "country, expected",
    [
        ("US", [examples.A_SERVER_US]),
        ("DE", [examples.A_SERVER_DE]),
        ("AR", []),
    ],
)
def test_filter_by_country(country, expected):
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_SERVER_US)
    fake_client.add_server(examples.A_SERVER_DE)

    proton_vpn = ProtonVPN(client=fake_client)

    actual = list(
        proton_vpn.get_servers().filter(
            lambda server: server.exit_country.upper() == country.upper()
        )
    )
    assert actual == expected


def test_sort_by_country():
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_SERVER_US)
    fake_client.add_server(examples.A_SERVER_DE)
    fake_client.add_server(examples.A_SERVER_AR)

    proton_vpn = ProtonVPN(client=fake_client)

    actual = (
        proton_vpn.get_servers().sort_by(lambda server: server.exit_country).as_list()
    )
    expected = [
        examples.A_SERVER_AR,
        examples.A_SERVER_DE,
        examples.A_SERVER_US,
    ]
    assert actual == expected


def test_filter_and_sort():
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_SERVER_US)
    fake_client.add_server(examples.AN_OFFLINE_SERVER)
    fake_client.add_server(examples.A_HIGH_SCORE_SERVER)

    proton_vpn = ProtonVPN(client=fake_client)

    actual = (
        proton_vpn.get_servers()
        .filter(lambda server: server.is_online())
        .sort_by(lambda server: server.exit_country)
        .filter(lambda server: server.score < 1)
        .as_list()
    )
    expected = [examples.A_SERVER_US]
    assert actual == expected


def test_search_p2p_servers():
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_P2P_SERVER)
    fake_client.add_server(examples.A_SERVER_DE)
    fake_client.add_server(examples.A_SERVER_US)

    proton_vpn = ProtonVPN(client=fake_client)

    actual = (
        proton_vpn.get_servers()
        .filter(lambda server: Feature.P2P in server.features)
        .as_list()
    )
    expected = [examples.A_P2P_SERVER]
    assert actual == expected


def test_refresh():
    fake_client = FakeProtonApiClient()
    fake_client.add_server(examples.A_SERVER_US)
    fake_client.add_server(examples.A_SERVER_DE)

    proton_vpn = ProtonVPN(client=fake_client)

    # 1. same servers than the api
    actual = proton_vpn.get_servers().as_list()
    expected = [examples.A_SERVER_US, examples.A_SERVER_DE]
    assert actual == expected

    # 2. add a new server in the api, but we should no see it until the refresh
    fake_client.add_server(examples.A_SERVER_AR)
    actual = proton_vpn.get_servers().as_list()
    expected = [examples.A_SERVER_US, examples.A_SERVER_DE]
    assert actual == expected

    # 3. we refreshed the server list, we should view the new server
    proton_vpn.refresh()
    actual = proton_vpn.get_servers().as_list()
    expected = [examples.A_SERVER_US, examples.A_SERVER_DE, examples.A_SERVER_AR]
    assert actual == expected
