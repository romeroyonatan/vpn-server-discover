import pytest
import requests
import responses

from protonvpn.clients import ProtonHTTPApiClient
from tests import examples


@responses.activate
def test_there_are_no_logical_servers():
    responses.get(
        examples.URL_LOGICAL_SERVERS, json=examples.RESPONSE_LOGICAL_SERVERS_EMPTY
    )

    client = ProtonHTTPApiClient(base_url=examples.A_BASE_URL)

    actual = client.get_servers()
    expected = []
    assert actual == expected


@responses.activate
def test_there_is_a_virtual_server():
    responses.get(
        examples.URL_LOGICAL_SERVERS, json=examples.RESPONSE_LOGICAL_SERVERS_1
    )

    client = ProtonHTTPApiClient(base_url=examples.A_BASE_URL)

    actual = client.get_servers()
    expected = [examples.A_SERVER]
    assert actual == expected


@responses.activate
def test_there_are_many_virtual_servers():
    logical_servers_json = examples.DATA_DIR / "logical-servers.json"
    responses.get(examples.URL_LOGICAL_SERVERS, body=logical_servers_json.read_bytes())

    client = ProtonHTTPApiClient(base_url=examples.A_BASE_URL)
    servers = client.get_servers()

    assert len(servers) == 2847


@responses.activate
def test_error_response():
    responses.get(examples.URL_LOGICAL_SERVERS, status=400)

    client = ProtonHTTPApiClient(base_url=examples.A_BASE_URL)

    with pytest.raises(requests.RequestException):
        client.get_servers()
