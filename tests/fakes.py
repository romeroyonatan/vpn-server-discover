from protonvpn.clients import ProtonApiClient
from protonvpn.servers import LogicalServer


class FakeProtonApiClient(ProtonApiClient):
    def __init__(self):
        self._servers = []

    def add_server(self, a_server: LogicalServer):
        self._servers.append(a_server)

    def get_servers(self) -> list[LogicalServer]:
        return list(self._servers)
