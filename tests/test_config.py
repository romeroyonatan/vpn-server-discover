from protonvpn import config
from tests import examples


def test_read_base_url_from_env(monkeypatch):
    monkeypatch.setenv("PROTONVPN_BASE_URL", examples.A_BASE_URL)
    assert config.get_base_url() == examples.A_BASE_URL


def test_read_base_url_from_user_settings(tmp_path, monkeypatch):
    user_settings = tmp_path / "config.ini"
    monkeypatch.setattr(config, "USER_SETTINGS_PATH", user_settings)

    user_settings.write_text(
        """
        [ProtonVPN]
        base_url = https://www.example.com
        """
    )

    assert config.get_base_url() == "https://www.example.com"


def test_cache_seconds_from_env(monkeypatch):
    monkeypatch.setenv("PROTONVPN_CACHE_SECONDS", "3600")
    assert config.get_cache_seconds() == 3600


def test_read_cache_seconds_from_settings(tmp_path, monkeypatch):
    user_settings = tmp_path / "config.ini"
    monkeypatch.setattr(config, "USER_SETTINGS_PATH", user_settings)

    user_settings.write_text(
        """
        [ProtonVPN]
        cache_seconds = 3600
        """
    )

    assert config.get_cache_seconds() == 3600


def test_precedence(tmp_path, monkeypatch):
    user_settings = tmp_path / "config.ini"
    monkeypatch.setattr(config, "USER_SETTINGS_PATH", user_settings)
    monkeypatch.setenv("PROTONVPN_BASE_URL", examples.A_BASE_URL)

    user_settings.write_text(
        """
        [ProtonVPN]
        cache_seconds = 3600
        base_url = https://www.example.com
        """
    )

    assert config.get_base_url() == examples.A_BASE_URL  # from env
    assert config.get_cache_seconds() == 3600  # from config
