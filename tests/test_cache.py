from unittest import mock

import freezegun

from protonvpn.cache import Cache


def test_get_data_from_source():
    cache = Cache(lambda: [1])
    assert cache.get() == [1]


def test_should_not_call_the_source_twice():
    a_source = mock.Mock(
        side_effect=[
            [1],
            AssertionError("The cache instance should not call this function twice"),
        ]
    )

    cache = Cache(a_source)

    assert cache.get() == [1]
    assert cache.get() == [1]


def test_refresh_the_cache_manually():
    a_source = mock.Mock(side_effect=[[1], [1, 2]])

    cache = Cache(a_source)
    assert cache.get() == [1]

    cache.refresh()
    assert cache.get() == [1, 2]


def test_cache_expiration():
    with freezegun.freeze_time("1970-01-01T00:00:00") as frozen_datetime:
        a_source = mock.Mock(side_effect=[[1], [1, 2]])

        cache = Cache(a_source, expiration_seconds=1)
        assert cache.get() == [1]

        # The cache is expired, and we should retrieve the data again
        frozen_datetime.tick()
        assert cache.get() == [1, 2]


def test_cache_is_not_expired_yet():
    with freezegun.freeze_time("1970-01-01T00:00:00") as frozen_datetime:
        a_source = mock.Mock(side_effect=[[1], [1, 2]])

        cache = Cache(a_source, expiration_seconds=2)
        assert cache.get() == [1]

        # The cache is not expired yet. We should not retrieve the data again
        frozen_datetime.tick()
        assert cache.get() == [1]
