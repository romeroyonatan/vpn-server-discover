import pathlib

from protonvpn.servers import LogicalServer

DATA_DIR = pathlib.Path(__file__).parent / "data"

A_SERVER_US_DICT = {
    "Name": "TEST#1",
    "EntryCountry": "US",
    "ExitCountry": "US",
    "Domain": "test-01.protonvpn.net",
    "Tier": 2,
    "Features": 24,
    "Region": None,
    "City": "Springfield",
    "Score": 1.27e-3,
    "HostCountry": None,
    "ID": "XXX-00000000000000000000000000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxx==",
    "Location": {"Lat": 64.13, "Long": -21.93},
    "Status": 1,
    "Servers": [
        {
            "EntryIP": "111.111.111.1",
            "ExitIP": "222.222.222.222",
            "Domain": "test-01.protonvpn.net",
            "ID": "XXXXXXXXXXXXXXXXXXXXXXXXX_0000000000000000_xxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxx-XXXX==",
            "Label": "2",
            "X25519PublicKey": "xxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxxxxxxxxxxxxx=",
            "Generation": 0,
            "Status": 1,
            "ServicesDown": 0,
            "ServicesDownReason": None,
        }
    ],
    "Load": 22,
}

A_SERVER_DE_DICT = {
    "Name": "TEST#2",
    "EntryCountry": "DE",
    "ExitCountry": "DE",
    "Domain": "test-02.protonvpn.net",
    "Tier": 0,
    "Features": 1,
    "Region": None,
    "City": "Berlin",
    "Score": 1.27e-3,
    "HostCountry": None,
    "ID": "YYY-00000000000000000000000000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxx==",
    "Location": {"Lat": 64.13, "Long": -21.93},
    "Status": 0,
    "Servers": [
        {
            "EntryIP": "11.11.11.11",
            "ExitIP": "22.22.22.22",
            "Domain": "test-02.protonvpn.net",
            "ID": "YYYYYYYYYYYYYYYYYYYYYYYYY_0000000000000000_xxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxx-XXXX==",
            "Label": "2",
            "X25519PublicKey": "xxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxxxxxxxxxxxxx=",
            "Generation": 0,
            "Status": 1,
            "ServicesDown": 0,
            "ServicesDownReason": None,
        }
    ],
    "Load": 11,
}

A_SERVER_AR_DICT = {
    "Name": "TEST#3",
    "EntryCountry": "AR",
    "ExitCountry": "AR",
    "Domain": "test-01.protonvpn.net",
    "Tier": 1,
    "Features": 5,
    "Region": None,
    "City": "Bariloche",
    "Score": 1,
    "HostCountry": None,
    "ID": "ZZZ-00000000000000000000000000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxx==",
    "Location": {"Lat": 64.13, "Long": -21.93},
    "Status": 1,
    "Servers": [
        {
            "EntryIP": "1.1.1.1",
            "ExitIP": "2.2.2.2",
            "Domain": "test-03.protonvpn.net",
            "ID": "ZZZZZZZZZZZZZZZZZZZZZZZZZ_0000000000000000_xxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxx-XXXX==",
            "Label": "2",
            "X25519PublicKey": "xxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxxxxxxxxxxxxx=",
            "Generation": 0,
            "Status": 1,
            "ServicesDown": 0,
            "ServicesDownReason": None,
        },
        {
            "EntryIP": "3.3.3.3",
            "ExitIP": "4.4.4.4",
            "Domain": "test-04.protonvpn.net",
            "ID": "ZZZZZZZZZZZZZZZZZZZZZZZZZ_1111111111111111_xxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxx-XXXX==",
            "Label": "2",
            "X25519PublicKey": "xxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxxxxxxxxxxxxx=",
            "Generation": 0,
            "Status": 1,
            "ServicesDown": 1,
            "ServicesDownReason": None,
        },
    ],
    "Load": 11,
}

A_BASE_URL = "https://test.api.protonvpn.ch"
URL_LOGICAL_SERVERS = f"{A_BASE_URL}/vpn/logicals"
RESPONSE_LOGICAL_SERVERS_EMPTY = {"Code": 1000, "LogicalServers": []}
RESPONSE_LOGICAL_SERVERS_1 = {"Code": 1000, "LogicalServers": [A_SERVER_US_DICT]}

A_SERVER_US = LogicalServer.from_dict(A_SERVER_US_DICT)
A_SERVER_DE = LogicalServer.from_dict(A_SERVER_DE_DICT)
A_SERVER_AR = LogicalServer.from_dict(A_SERVER_AR_DICT)

AN_OFFLINE_SERVER = A_SERVER_DE
A_HIGH_SCORE_SERVER = A_SERVER_AR
A_P2P_SERVER = A_SERVER_AR
A_SERVER = A_SERVER_US
