import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--integration-tests",
        action="store_true",
        help="Run the integration tests.",
    )


def pytest_runtest_setup(item):
    is_integration_test = any(item.iter_markers(name="integration_test"))
    if is_integration_test and not item.config.getoption("--integration-tests"):
        pytest.skip(
            "Skipping integration test. Add parameter --integration-tests to run this test"
        )
