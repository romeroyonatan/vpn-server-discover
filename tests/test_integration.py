"""Integration tests

Use real servers.

Run these tests with
```sh
pytest --integration-tests
```
"""
import pytest

from protonvpn import ProtonVPN
from protonvpn.servers import Feature


@pytest.mark.integration_test
def test_get_servers():
    proton_vpn = ProtonVPN()
    servers = proton_vpn.get_servers().as_list()
    assert len(servers) > 10

    us_servers = (
        proton_vpn.get_servers()
        .filter(lambda server: server.exit_country == "US")
        .as_list()
    )
    assert len(us_servers) > 1

    streaming_servers = (
        proton_vpn.get_servers()
        .filter(lambda server: Feature.STREAMING in server.features)
        .filter(lambda server: server.is_online())
        .filter(lambda server: Feature.P2P in server.features)
        .sort_by(lambda server: server.exit_country)
        .as_list()
    )
    assert len(streaming_servers) > 1

    free_servers = (
        proton_vpn.get_servers().filter(lambda server: server.tier == 0).as_list()
    )
    assert len(free_servers) > 1
