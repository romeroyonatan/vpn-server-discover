# Proton VPN Client
> Discover Proton VPN servers

## Install dependencies
```sh
python3.11 -m venv .venv
pip install -r dev-requirements.txt
pip install -e .
```

## Run
```sh
protonvpn-gui
```

## Configuration
### Environment variables
- `PROTONVPN_BASE_URL`: Base URL of the Proton VPN API (default: https://api.protonvpn.ch)
- `PROTONVPN_CACHE_SECONDS`: Expiration time in seconds (default 600 seconds)

### User settings
Location `~/.config/protonvpn/config.ini`

```ini
[ProtonVPN]
base_url = https://api.protonvpn.ch
cache_seconds = 600
```

## Develop
### Docker
```sh
make docker-build-image  # build the docker image
make docker-test  # run the tests in a container
make docker-integration-tests  # run integration tests in a container
```
### Update dependencies
1. Update pyproject.toml with the new dependencies
2. Run
    ```sh
    make update-dependencies
    ```


# Architecture
The entrypoint of the Proton VPN services is `protonvpn.ProtonVPN`

```python
from protonvpn import ProtonVPN
proton = ProtonVPN()
proton.get_servers().as_list()
```

We can also filter and sort the server list for any criteria
```python
(
    proton.get_servers()
        .filter(lambda server: server.is_online())
        .sort_by(lambda server: server.name)
        .as_list()
)
```

## GUI
The GUI was developed using GTK3. It needs more work.

Missing features:
- Error handling
- Pagination
- Show status (a loading spinner for example)
- It should load the servers in background and not block the main thread
- Scrolling
- Automated tests
