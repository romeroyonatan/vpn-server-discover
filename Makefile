update-dependencies:
	pip-compile -o requirements.txt --strip-extras pyproject.toml 
	pip-compile -o dev-requirements.txt --extra dev pyproject.toml


install-dev-dependencies:
	pip install -r dev-requirements.txt
	pip install -e .

test:
	pytest -v --durations=3


build:
	pip install pyinstaller
	pyinstaller src/protonvpn/gui.py -y

integration-tests:
	pytest --integration-tests -v --durations=3

docker-build-image:
	docker build . -t romeroyonatan/protonvpn:dev

docker-build:
	docker run --rm -v "$(PWD):/app" romeroyonatan/protonvpn:dev make build

docker-test:
	docker run --rm -v "$(PWD):/app" romeroyonatan/protonvpn:dev make test

docker-integration-test:
	docker run --rm -v "$(PWD):/app" romeroyonatan/protonvpn:dev make integration-tests

lint:
	ruff check .

clean:
	find -name '*.pyc' -delete
	find -name '__pycache__' -delete
	rm -rf build/ dist/ htmlcov/ src/proton_vpn.egg-info proton_vpn.egg-info .coverage .pytest_cache/ .ruff_cache/ tests/.pytest_cache

.PHONY: update-dependencies install-dev-dependencies test integration-tests lint docker-integration-tests docker-build-image docker-test clean docker-build build
