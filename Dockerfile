# Docker image for development
# Features:
#  - build the desktop app 
#  - run the tests
FROM python:3.11.4-bullseye
WORKDIR /app
RUN apt-get update \
    && apt-get install -y \
        python3-gi=3.38.0-2 \
        libcairo2-dev=1.16.0-5 \
        libgirepository1.0-dev=1.66.1-1+b1

COPY dev-requirements.txt .
RUN pip install -r dev-requirements.txt

COPY pyproject.toml README.md .
COPY src/ ./src/
RUN pip install -e .
